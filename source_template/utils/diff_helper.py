#!/usr/bin/python
#
# use: utils/diff_helper.py|xargs bzr diff
#

from bzrlib import workingtree
from subprocess import call

wt = workingtree.WorkingTree.open('.')
changes=wt.changes_from(wt.basis_tree())

for add in changes.added:
    print add[0]

for rm in changes.removed:
    print rm[0]

