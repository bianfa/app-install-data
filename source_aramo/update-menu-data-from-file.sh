#!/bin/sh

if [ ! -f "$1" ]; then
    echo "need a valid filename"
    exit 1
fi
MENU=$(readlink -f "$1")

UTILDIR=utils/
TOPDIR=./
TARGETDIR=./menu-data/
HERE=$PWD

# cleanup
rm -f $TARGETDIR/*.desktop 
rm -f $TARGETDIR/icons/* 

# unpack and shuffle files around
(cd $TOPDIR/menu-data ; 
  tar xvf  $MENU ; 
  mv desktop/* .;
  rmdir desktop;
)
./pre-build.sh

# post-process
bzr add $TARGETDIR/*.desktop
bzr add $TARGETDIR/*.scope
bzr add $TARGETDIR/icons/*

# update the codec information based on the Packages file output
$UTILDIR/gst-add.py ./menu-data-codecs/*.desktop

