#!/usr/bin/python

import glob

pkgnames_to_desktop = {}

for fname in glob.glob("menu-data/*.desktop"):
    with open(fname) as f:
        for line in f.readlines():
            line = line.strip()
            if line.startswith("X-AppInstall-Package"):
                pkgname = line.split("=")[-1]
                if not pkgname in pkgnames_to_desktop:
                    pkgnames_to_desktop[pkgname] = []
                pkgnames_to_desktop[pkgname].append(fname)

for pkgname, desktop_files in sorted(pkgnames_to_desktop.iteritems(),
                                     cmp=lambda a,b: -cmp(len(a[1]), 
                                                          len(b[1]))):
    if len(desktop_files) > 1:
        print("%s: %s" % (pkgname, desktop_files))
        
